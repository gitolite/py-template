Pytemplate

Little Pythonprogram which can handle a template folder according to your needs.

Goal: Setup your latex/programming templates including a git init by a shell-oneliner.

required:
* ConfigParser
* OptionParser
* git
