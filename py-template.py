# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 20:47:53 2013

@author: koeart

conffile = filename
config = SafeConfigParser() object
"""

__author__ = "Paul Schwanse <ps@zwoelfelf.org>"
__version__ = "0.0.1"
__date__ = "Tue 02 Jul 2013 23:23:42 CET"
__copyright__ = "Copyright (c) 2013 Paul Schwanse"
__license__ = "Python"


import os
import shutil
import sys
from optparse import OptionParser
from ConfigParser import SafeConfigParser

    



# hier muesste man eigtl. die headline nur den "name" key rauspopeln

def make_default_config(conffile='config.cfg'):
    """
    Creates the basic but default configuration file.
    
    To add new things to the config, setup dict()s. the key "sectionname" is essential, because the actual sectionname of the configurationfile is made up from this.
    
    Standard Config:
    -----
    #all path except templatedir are considered to be relative to templatedir
    [DEFAULT]   ; in this section all defaults are set
    templatedir = ~/templates/
    prepre=pre- ;uses expansion
    git=False   ;setup git -> use git section
    
    [GIT]   ; to be implemented
    gituser=koeart
    gitdir=
    giturl=
    
    #sets all dirs.
    #everything is optional. Please see Documentation for supported things!
    [latex-beamer]
    directory=latex-beamer
    preambeldir=latex-preambel
    latexengine=lualatex
    preambel=%(prepre)s%(latexengine)s
        
    """
    
    DEFAULT = dict(templatedir="~/templates/",
                   prepre="pre-",
                   git=False)
    git = dict(sectionname="git",
               gitdir="~./git",
               gituser="yourgituser",
               gitemail="<you@provider.org>",
               giturl="yourIP")
    latexbeamer = dict(sectionname="latexbeamer",
                       directory="latex-beamer",
                       preambledir="latex-preambel",
                       latexengine="lualatex",
                       preambel="%(prepre)s%(latexengine)s")
    
    sections=(git, latexbeamer)
    config = SafeConfigParser(DEFAULT)
    
    for section in sections:
        config.add_section(section['sectionname'])
        for key, value in section.items():
            config.set(section['sectionname'], key, value)
             
    with open(conffile, 'w') as conffile:
        config.write(conffile)
        
    show_config(config)
          

def show_config(config, *secs):
    line =''
    if secs:
        sections = secs
    else:
        sections = config.sections()
    for section in sections:
        line += "\n["+ section +"]\n"
        for option, value in config.items(section):
            line += option + " = " + value + "\n"
    print line
    print "Attention! Default values are written here as well! Don't wonder!"
    
def copy_template(config, template, outdir, verbose=False):
    cwd = os.getcwd()
    #print cwd
    parameters = config.items(template)
    directory = dict(parameters)['directory']
    preambledir = dict(parameters)['preambledir']
    latexengine = dict(parameters)['latexengine']
    preamble = dict(parameters)['preambel']
    sourcedir = dict(parameters)['templatedir']
    sourcedir = os.path.expanduser(sourcedir)
    source = shutil.abspath(sourcedir+directory)
    print source
    try:
        shutil.copytree(sourcedir + directory, outdir)
        message = "success"
    except:
        message = "uups"
        pass
    
    return message
    
    
def init_parser():
    """Parse Command Line Options and Arguments"""
    parser = OptionParser()
    
    parser.add_option(
        "--make-default-config",
        dest="makedefaultconf",
        help=u"Creates a default Config in the current working directory. Default: config.cfg",
        default=False,
        action="store_true")
        
    parser.add_option(
        "--conffile",
        "-c",
        dest="conffile",
        help=u"$metavar from which config data should be read. Default: $default",
        default="config.cfg",
        metavar="FILE")
        
    parser.add_option(
        "--show-config",
        "-s",
        dest="showconfig",
        help=u"shows current config. If an Argument if an argument, thus section, is supplied, list only section",
        default=False,
        action="store_true")
        
    parser.add_option(
        "--outdir",
        "-o",
        dest="outdir",
        help=u"set output directory to DIR",
        default="./",
        metavar="DIR")
    
    parser.add_option(
        "--verbose",
        "-v",
        dest="verbose",
        help=u"blabber an",
        default=False,
        action="store_true",
        )
    
    options, args = parser.parse_args()
    return options, args
    
def main():
    
    #read command line options and arguemnts
    options,args = init_parser()
    print type(options)
    
    #check if (the specified) config exists, otherwise: create one
    if options.makedefaultconf:
        make_default_config(options.conffile)
        print "created " + options.conffile
        sys.exit()
    
    #try to open conffile    
    try:
        config = SafeConfigParser()
        config.readfp(open(options.conffile),'r')
        if options.showconfig:
            show_config(config)
    except:
        print "problems occured while reading configfile"

 
    #now look at the arguemnt what to do.
    # make sure there is only one
    
    if len(args)>1:
        print "Too many arguments! I cannot do that! Please provide one single templatething you want to have!"
    elif len(args) == 0:
        print "Not enough arguments :("
                  
    if len(args)==1 & config.has_section(args[0]):
        argument = args[0]
        meldung = copy_template(config, argument, options.outdir, options.verbose)
        print meldung
        sys.exit()
    else:
        print "template unknown! The following templates can be used:" + config.sections()
        sys.exit()


    


if __name__ == "__main__":
    main()
    
    

